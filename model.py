import json
import re
import os

class Model:
    card_set        =   []
    card_set_name   =   ''

    def __init__(self):
        super(Model, self).__init__()

        self.card_set      =   []

    def load_card_set(self, file_path):

        self.card_set_name  =   re.findall(r'.*/(.+?)\.json', file_path)[0]

        with open(file_path) as data_stream:
            self.card_set  =   json.load(data_stream)

        #sort the list the front side as key and case insensitive
        self.card_set.sort(key = lambda card: card['front'].casefold())

    def save_card_set(self):
        if not os.path.exists('./Card Sets/'):
            os.makedirs('./Card Sets/')

        with open(f'./Card Sets/{self.card_set_name}.json', 'w') as f:
            f.write(json.dumps(self.card_set, ensure_ascii = False, indent = 4))

    # take a new card, find its place in the sorted list
    def find_card_index(self, new_card):
        SAME_CARD_FLAG = -1

        if (self.is_string_bigger(self.card_set[0]['front'], new_card['front'])):
            if (self.is_the_same_card(self.card_set[0], new_card)):
                # if is the same card, do return a flag
                return SAME_CARD_FLAG
            else:
                self.card_set.insert(0, new_card)
                return 0

        for i in range(1, len(self.card_set)):
            #if card[i-1] < new_card < card[i], return i

            if ((self.is_string_bigger(new_card['front'], self.card_set[i-1]['front'])) and 
                (self.is_string_bigger(self.card_set[i]['front'], new_card['front']))):
                    if (self.is_the_same_card(self.card_set[i], new_card)):
                        return SAME_CARD_FLAG
                    else:
                        self.card_set.insert(i, new_card)
                        return i

        if (self.is_the_same_card(self.card_set[-1], new_card)):
            return SAME_CARD_FLAG

        self.card_set.append(new_card)
        return len(self.card_set) + 1
    
    @staticmethod
    def is_string_bigger(str1, str2):
        return (str1.casefold() >= str2.casefold())

    @staticmethod
    def is_the_same_card(card1, card2):
        return ((card1['front'] == card2['front'])  and 
                (card1['hint']  == card2['hint'])   and
                (card1['back']  == card2['back']))