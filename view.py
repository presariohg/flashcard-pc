from PyQt5.uic          import  loadUi
from PyQt5.QtCore       import  Qt
from PyQt5.QtWidgets    import  QMainWindow, QMessageBox, QFileDialog

from class_definitions  import  List_Widget_Item

class View(QMainWindow):
    def __init__(self):
        super(View, self).__init__()
        loadUi('flashcard.ui', self)

        self.setFixedSize(self.size())
        self.ckb_edit_mode.setChecked(True)


    def show_chosen_card(self):
        chosen_card = self.lw_cards.item(self.lw_cards.currentRow())
        
        self.te_front.setText(chosen_card.front)
        self.te_hint.setText(chosen_card.hint)
        self.te_back.setText(chosen_card.back)

        self.te_front.setAlignment(Qt.AlignCenter)

    def delete_item(self, delete_index):
        self.lw_cards.takeItem(delete_index)
        self.show_chosen_card()

    def load_card_set(self, card_set):
        # delete old card list
        while (self.lw_cards.count() > 0):
            self.lw_cards.takeItem(0)

        # load new card list
        for card in card_set:
            self.lw_cards.addItem(List_Widget_Item(card))

    def insert_card(self, card, index):
        self.lw_cards.insertItem(index, List_Widget_Item(card))

    def prepare_new_card(self):
        self.te_front.setText('')
        self.te_hint.setText('')
        self.te_back.setText('')

        self.te_front.setAlignment(Qt.AlignCenter)

    def closeEvent(self, event):
        is_confirm_quit     =   QMessageBox.question(self, 'Confirm', 'All unsaved changes will be deleted. Confirm quit?')

        if (is_confirm_quit ==  QMessageBox.Yes):
            event.accept()
        else:
            event.ignore()
 
    def get_card_set_path(self):
        # getOpenFileName returns a tuple: ('./Card Sets/cards.json', 'Card set (*.json)')
        # its first element is the chosen file's path

        return QFileDialog.getOpenFileName(self, "Choose Card Set", "./Card Sets", "Card set (*.json)")[0]