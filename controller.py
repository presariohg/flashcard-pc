from PyQt5.QtWidgets        import  QApplication

from view                   import  View
from model                  import  Model

import sys

#TODO new card set option

class Controller:

    def __init__(self, view, model):

        self.view       =   view
        self.model      =   model

        self.view.lw_cards.itemClicked.connect(self.view.show_chosen_card)

        self.view.bt_new_card.clicked.connect(self.prepare_new_card)
        self.view.bt_reload.clicked.connect(self.load_card_set)
        self.view.bt_delete.clicked.connect(self.delete_item)
        self.view.bt_save.clicked.connect(self.save_item)

        self.view.act_close_set.triggered.connect(self.close_card_set)
        self.view.act_open.triggered.connect(self.open_card_set)
        self.view.act_save.triggered.connect(self.save_card_set)

        self.view.ckb_edit_mode.clicked.connect(self.toggle_edit_mode)

    def delete_item(self):
        delete_index    =   self.view.lw_cards.currentRow()
        
        del(self.model.card_set[delete_index])

        self.view.delete_item(delete_index)

    def load_card_set(self, file_path):

        self.model.load_card_set(file_path)
        self.view.load_card_set(self.model.card_set)
        self.view.setWindowTitle(f'Flashcard - {self.model.card_set_name}')

    def save_item(self):
        SAME_CARD_FLAG = -1

        front       =   self.view.te_front.toPlainText()
        hint        =   self.view.te_hint.toPlainText()
        back        =   self.view.te_back.toPlainText()
        
        new_card    =   {
                        'front' : front,
                        'hint'  : hint,
                        'back'  : back,
                        }
        new_index   =   self.model.find_card_index(new_card)

        if (new_index == SAME_CARD_FLAG):
            return

        self.view.insert_card(new_card, new_index)

    def toggle_edit_mode(self):
        if (self.view.ckb_edit_mode.isChecked()):

            self.view.bt_delete.setEnabled(False)
            self.view.bt_save.setEnabled(False)

            self.view.te_front.setReadOnly(False)
            self.view.te_hint.setReadOnly(False)
            self.view.te_back.setReadOnly(False)
        else:

            self.view.bt_delete.setEnabled(True)
            self.view.bt_save.setEnabled(True)

            self.view.te_front.setReadOnly(True)
            self.view.te_hint.setReadOnly(True)
            self.view.te_back.setReadOnly(True)

    def prepare_new_card(self):
        self.save_item()
        self.view.prepare_new_card()

    def open_card_set(self):
        file_path   =   self.view.get_card_set_path()
        
        self.load_card_set(file_path)

    def close_card_set(self):
        self.model.card_set         =   []
        self.model.card_set_name    =   ''

        self.view.prepare_new_card()
        self.view.load_card_set([])
        self.view.setWindowTitle('Flashcard')

    def save_card_set(self):
        self.model.save_card_set()

if (__name__    == "__main__"):

    app         =   QApplication(sys.argv)
    view        =   View()
    model       =   Model()

    controller  =   Controller(view, model)
    controller.view.show()

    sys.exit(app.exec_())