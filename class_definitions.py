from PyQt5.QtWidgets        import QListWidgetItem

class List_Widget_Item(QListWidgetItem):
    def __init__(self, card):
        super(List_Widget_Item, self).__init__()
        
        self.setText(card['front'])

        self.front    =   card['front']
        self.hint     =   card['hint']
        self.back     =   card['back']
